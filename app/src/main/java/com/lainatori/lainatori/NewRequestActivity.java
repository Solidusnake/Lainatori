package com.lainatori.lainatori;

/**
 * Created by Wam on 11.10.2017.
 */

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import SharedData.CommonVariables;
import entity.OpenRequest;

public class NewRequestActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private OpenRequest openRequest = null;
    private Calendar date = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_request_layout);

        Calendar dateNow = Calendar.getInstance();

        dateNow.getTimeInMillis();

        openRequest = new OpenRequest();

        date = Calendar.getInstance();
    }

    public void onClick(View view) {

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.up_from_bottom );
        view.startAnimation(animation);

        switch (view.getId()) {
            case R.id.period_of_validity:

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        this, NewRequestActivity.this, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();

                break;

            case R.id.send_request:

                try {
                    EditText titleText = (EditText) findViewById(R.id.title_text);

                    titleText.setBackgroundColor(Color.WHITE);

                    String title = titleText.getText().toString();

                    if (title.isEmpty()) {
                        titleText.setBackgroundColor(Color.RED);
                        throw new Exception("Title cannot be empty");
                    }

                    openRequest.setName(title);

                    EditText descriptionText = (EditText) findViewById(R.id.description_text);

                    descriptionText.setBackgroundColor(Color.WHITE);

                    String description = descriptionText.getText().toString();

                    if (description.isEmpty()) {
                        descriptionText.setBackgroundColor(Color.RED);
                        throw new Exception("Description cannot be empty");

                    }

                    openRequest.setDescription(description);

                    openRequest.setTime(date.getTimeInMillis());

                    Calendar dateNow = Calendar.getInstance();

                    openRequest.setId(dateNow.getTimeInMillis());

                    CommonVariables.openRequests.add(openRequest);

                    finish();
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        date.set(Calendar.YEAR, year);

        date.set(Calendar.MONTH, month);

        date.set(Calendar.DAY_OF_MONTH, dayOfMonth);

    }
}

