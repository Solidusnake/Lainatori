package com.lainatori.lainatori;

import android.content.Intent;
import android.os.Bundle;

import SharedData.CommonVariables;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//for image
        //bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.settings_icon));

        if (CommonVariables.isLogIn) {
            Intent intent = new Intent(MainActivity.this, IfLogInNeededActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int loop = 0;

                while (true) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    loop++;

                    if (loop > 2) {
                        Intent intent = new Intent(MainActivity.this, StartNotificatonsActivity.class);
                        startActivity(intent);
                        finish();
                        return;
                    }
                }
            }
        });

        thread.start();
    }
}
