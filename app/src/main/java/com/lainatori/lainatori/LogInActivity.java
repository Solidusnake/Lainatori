package com.lainatori.lainatori;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import SharedData.CommonVariables;

/**
 * Created by Wam on 2.10.2017.
 */

public class LogInActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_layout);

        Button logIn = (Button) findViewById(R.id.log_in);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                startActivity(intent);
                CommonVariables.isLogIn = false;
                Toast.makeText(getApplicationContext(), getText(R.string.user_log_in) + " !!", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }
}
