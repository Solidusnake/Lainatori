package com.lainatori.lainatori;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Calendar;

import SharedData.CommonVariables;
import adapter.MyArrayAdapter;
import entity.OpenRequest;

/**
 * Created by Wam on 29.9.2017.
 */

public class StartNotificatonsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_notifications_layout);

        super.setViews();

        CommonVariables.openRequests.clear();

        Calendar dateNow = Calendar.getInstance();

        CommonVariables.openRequests.add(new OpenRequest(1, "Apple iPhone 5", "Want a rent iPhone 5", "September 23, 2018", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(2, "Samsung GS 3850", "Need fot funny working Samsung GS3850", "February 9, 2019", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(3, "Tunturi Bicycle", "Need for fully working Tunturi bicycle. I return it totally greased", "April 27, 2019", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(4, "Helkama bicycle", "They say that Helkama takes me anywhere. I want to find out where I end up.", "September 15, 2019", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(5, "Renaul Laguna Break 97", "Renault Laguna Break 97 for weekend loan. Its better working fine. ", "October 26, 2019", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(6, "Lada Samara", "I want to go Lapland with Lada Samara. I dont want push whole trip", "May 20, 2018", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(7, "GoPro camera", "I want to lent GoPro action camera", "December 6, 2017", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(8, "Hammer", "Want to rent hammer", "February 22, 2016", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(9, "Tunturi XXLS", "Want to lent Tunturi XXLS mountain bike", "October 18, 2017", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(10, "Spoon", "want to lent big pourage spoon", "July 9, 2017", dateNow.getTimeInMillis()));
        CommonVariables.openRequests.add(new OpenRequest(11, "T-Cup", "I really need Chinese T-Cup", "October 31, 2017", dateNow.getTimeInMillis()));


        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(StartNotificatonsActivity.this, NewRequestActivity.class);

                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        ListView listView = (ListView) findViewById(R.id.notification_list);

        MyArrayAdapter adapter = new MyArrayAdapter(CommonVariables.openRequests, getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                OpenRequest openRequest = CommonVariables.openRequests.get(position);

                Intent intent = new Intent(StartNotificatonsActivity.this, ProductDetailActivity.class);
                intent.putExtra("myKey", openRequest.getId());
                startActivity(intent);
            }
        });
    }
}

