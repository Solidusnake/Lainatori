package com.lainatori.lainatori;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Wam on 2.10.2017.
 */

public class IfLogInNeededActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.if_login_needed_layout);

        Button logIn = (Button) findViewById(R.id.log_in);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IfLogInNeededActivity.this, LogInActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}


