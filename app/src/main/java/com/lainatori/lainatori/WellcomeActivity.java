package com.lainatori.lainatori;

import android.content.Intent;
import android.os.Bundle;

import SharedData.CommonVariables;

public class WellcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (CommonVariables.isLogIn) {
            Intent intent = new Intent(WellcomeActivity.this, IfLogInNeededActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int loop = 0;

                while (true) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    loop++;

                    if (loop > 10) {
                        Intent intent = new Intent(WellcomeActivity.this, StartNotificatonsActivity.class);
                        startActivity(intent);
                        finish();
                        return;
                    }
                }
            }
        });

        thread.start();
    }
}
