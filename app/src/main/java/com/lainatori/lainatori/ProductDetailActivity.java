package com.lainatori.lainatori;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;

import SharedData.CommonVariables;
import entity.OpenRequest;

/**
 * Created by Wam on 6.10.2017.
 */

public class ProductDetailActivity extends BaseActivity {

    OpenRequest openRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_layout);

        Bundle extras = getIntent().getExtras();
        Long id = extras.getLong("myKey");

        for (int i = 0; i < CommonVariables.openRequests.size(); i++) {
            if (CommonVariables.openRequests.get(i).getId() == id) {
                openRequest = CommonVariables.openRequests.get(i);
                break;
            }
        }

        TextView title = (TextView) findViewById(R.id.title);

        title.setText(openRequest.getName());

        TextView description = (TextView) findViewById(R.id.description);

        description.setText(openRequest.getDescription());

    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.message:


                break;
            case R.id.message_image:

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:123456789"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);

                break;

            case R.id.title:


                break;
        }
    }
}
