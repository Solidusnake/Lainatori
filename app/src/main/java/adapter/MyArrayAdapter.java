package adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lainatori.lainatori.R;

import java.util.ArrayList;
import java.util.Calendar;

import entity.OpenRequest;

/**
 * Created by Wam on 2.10.2017.
 */

public class MyArrayAdapter extends ArrayAdapter<OpenRequest> implements View.OnClickListener {

    Context mContext;
    private ArrayList<OpenRequest> dataSet;
    private int lastPosition = -1;

    public MyArrayAdapter(ArrayList<OpenRequest> data, Context context) {
        super(context, R.layout.image_between_views, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        OpenRequest openRequest = (OpenRequest) object;

        switch (v.getId()) {

        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        OpenRequest openRequest = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.image_between_views, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.txtDescription = (TextView) convertView.findViewById(R.id.description_text);
            viewHolder.publish = (TextView) convertView.findViewById(R.id.publish);

            result = convertView;

            final RelativeLayout upperBar = (RelativeLayout) convertView.findViewById(R.id.upper_bar);

            final RelativeLayout bottomBar = (RelativeLayout) convertView.findViewById(R.id.bottom_bar);

            final LinearLayout profile = (LinearLayout) convertView.findViewById(R.id.profile_view_3);
            ViewTreeObserver vto = result.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        result.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        result.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    //int width = result.getMeasuredWidth();
                    //int height = result.getMeasuredHeight();


                    //int width2 = upperBar.getMeasuredWidth();
                    int height2 = upperBar.getMeasuredHeight();

                    //image.setMinimumHeight(height2);
                    //image.setMinimumWidth(height2);


                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    lp.topMargin = height2 / 2;
                    //lp.addRule(); = Gravity.LEFT;

                    //lp.p(lp.topMargin,0,0,0);
                    //image.addRule(RelativeLayout.RIGHT_OF, tv1.getId());
                    profile.setLayoutParams(lp);


                }
            });

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top_animation);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.title.setText(openRequest.getName());
        viewHolder.txtDescription.setText(openRequest.getDescription());

        Calendar publishDay = Calendar.getInstance();

        publishDay.setTimeInMillis(openRequest.getPublish());

        Calendar dateNow = Calendar.getInstance();

        if (dateNow.get(Calendar.MONTH) == publishDay.get(Calendar.MONTH)) {
            if (dateNow.get(Calendar.DAY_OF_MONTH) == publishDay.get(Calendar.DAY_OF_MONTH)) {
                viewHolder.publish.setText(mContext.getText(R.string.today));
            } else if (dateNow.get(Calendar.DAY_OF_MONTH) > publishDay.get(Calendar.DAY_OF_MONTH)) {
                int difference = dateNow.get(Calendar.DAY_OF_MONTH) - publishDay.get(Calendar.DAY_OF_MONTH);
                if (difference == -1) {
                    viewHolder.publish.setText(mContext.getText(R.string.easterday));
                }
            }
        }


        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView txtDescription;
        TextView publish;
    }
}
