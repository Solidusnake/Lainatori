package entity;

/**
 * Created by Wam on 3.10.2017.
 */

public class BaseEntity {

    protected long id;
    protected String name;

    protected Long publish;

    public BaseEntity() {

    }


    public BaseEntity(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPublish() {
        return publish;
    }

    public void setPublish(Long publish) {
        this.publish = publish;
    }
}