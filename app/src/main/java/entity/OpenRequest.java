package entity;

/**
 * Created by Wam on 2.10.2017.
 */

public class OpenRequest extends BaseEntity {


    private String description;
    private String feature;

    private Long time;

    public OpenRequest() {
        super();
    }

    public OpenRequest(int id, String name, String type, String feature, Long publish) {
        super(id, name);
        this.description = type;
        this.feature = feature;
        this.publish = publish;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}